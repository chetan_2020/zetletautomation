# ZETLET AUTOMATION

## README

### STEPS:

1. Install Kubernetes using following link and use this kube config: https://docs.google.com/document/d/1jMlL8c4wS2D9-H7pO5gdjlaN4D8ZrQ7sv9_WoqputUY/edit

2. Install kubectl 

```
$ curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/$(uname | awk '{print tolower($0)}')/amd64/kubectl
$ chmod +x ./kubectl
$ sudo mv ./kubectl /usr/local/bin/kubectl
```

3. Save the above file in ~/.kube. Name the file as config 

4. Try checking using this command “kubectl get node“

5. Run the following command to run the server:

```
kubectl port-forward zetlet-automation-v3-c546d9787-wpp4s 3000 -n zeta-hack-2
```

6. Access from http://localhost:3000/

7. Use the automation tool to generate templates.
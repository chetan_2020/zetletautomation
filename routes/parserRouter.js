const express=require('express');
const bodyparser=require('body-parser');
const solver = require('../coreLogic/solver')

const parserRouter=express.Router();
parserRouter.use(bodyparser.json());
parserRouter.route('/')
.all((req,res,next) =>
{
    res.statusCode=200;
    res.setHeader('Content-Type','application/json');
    next();
})
.post((req,res,next)=>
{
    res.end(JSON.stringify(solver.parseMe(req.body)));
});

module.exports=parserRouter;
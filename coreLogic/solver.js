const ZetletParser = require('./parser')
const Element = require('./element')

module.exports.parseMe =(data) => {

    var zetletParser= new ZetletParser();
    var elements = [];

    Object.keys(data).forEach(key => {
        var element= new Element(
            parseInt(data[key].xCoordinate),
            parseInt(data[key].yCoordinate),
            parseInt(data[key].width),
            parseInt(data[key].height),
            data[key].id,
            data[key].zetletStyle,
            data[key].type,
            data[key].data)
        elements.push(element)
    })
    return zetletParser.parseInput(elements);
}

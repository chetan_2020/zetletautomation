const Element = require('./element')
class ZetletParser{
constructor(){
    this.arr = new Map();
    this.parent = new Map();
    this.maximumValue = 1000000;
}

addEdge(v, w) {
    this.arr.get(v).push(w);
    this.arr.get(w).push(v);
}

addVertex(v) {
    this.arr.set(v, []);
}

findNode(elements, sourceNode, type) {
    var minimumDistance = this.maximumValue;
    var parentNode = null;
    for (var i = 0; i < elements.length; i++) {
        var a = 0, b = 0;
        if (type === 0) {
            a = sourceNode.yCoordinate;
            b = elements[i].yCoordinate;
        } else if (type === 1) {
            a = elements[i].xCoordinate;
            b = sourceNode.xCoordinate;
        } else if (type === 2) {
            a = sourceNode.xCoordinate;
            b = elements[i].xCoordinate;
        } else if (type === 3) {
            a = elements[i].yCoordinate;
            b = sourceNode.yCoordinate;
        }
        if (sourceNode != elements[i] && b - a >= 0 && b - a <= minimumDistance) {
            minimumDistance = b - a;
            parentNode = elements[i];
        }
    }
    return parentNode;
}

getRootElement() {
    return new Element(0, 0, 0, 0, "rootElement", {}, "", {});
}

createGraph(elements) {
    elements.push(this.getRootElement());
    for (var i = 0; i < elements.length; i++) {
       this.addVertex(elements[i]);
    }
    for (var i = 0; i < elements.length; i++) {
       var right = this.findNode(elements, elements[i], 2);
       var bottom = this.findNode(elements, elements[i], 3);
       var top = this.findNode(elements, elements[i], 0);
       var left = this.findNode(elements, elements[i], 1);

       if (top !== null) {
           this.addEdge(elements[i], top);
       }
       if (left !== null) {
           this.addEdge(elements[i], left);
       }
       if (right !== null) {
           this.addEdge(elements[i], right);
       }
       if (bottom !== null) {
           this.addEdge(elements[i], bottom);
       }
   }
}

calculateParent(root, visited, currentParent) {
    if (visited.has(root)) {
        return;
    }
    if (currentParent !== null) {
        this.parent.set(root, currentParent);
    }
    visited.add(root);
    for (var i = 0; i < this.arr.get(root).length; i++) {
        this.calculateParent(this.arr.get(root)[i], visited, root);
    }
}
getLayout(source, par) {
    var layout = {};
    if (source.xCoordinate + source.width < par.xCoordinate) {
        if (par.id === "rootElement") {
            layout["alignParentRight"] = true;
        } else {
            layout["leftOf"] = par.id;
        }
        layout["marginRight"] = par.xCoordinate - source.xCoordinate - source.width;
    } else if (source.xCoordinate > par.xCoordinate + par.width) {
        if (par.id === "rootElement") {
            layout["alignParentLeft"] = true;
        } else {
            layout["rightOf"] = par.id;
        }
        layout["marginLeft"] = source.xCoordinate - par.xCoordinate - par.width;
    } else if (source.xCoordinate < par.xCoordinate) {
        layout["alignRight"] = par.id;
        if (par.xCoordinate + par.width > source.xCoordinate + source.width) {
            layout["marginRight"] = par.xCoordinate + par.width - source.xCoordinate - source.width;
        } else {
            layout["marginRight"] = source.xCoordinate + source.width - par.xCoordinate - par.width;
        }
    } else if (source.xCoordinate > par.xCoordinate) {
        layout["alignLeft"] = par.id;
        layout["marginLeft"] = source.xCoordinate - par.xCoordinate;
    } else {
        layout["alignLeft"] = par.id;
    }

    if (source.yCoordinate + source.height < par.yCoordinate) {
        if (par.id === "rootElement") {
            layout["alignParentBottom"] = true;
        } else {
            layout["above"] = par.id;
        }
        layout["marginBottom"] = par.yCoordinate - source.yCoordinate - source.height;
    } else if (source.yCoordinate > par.yCoordinate + par.height) {
        if (par.id === "rootElement") {
            layout["alignParentTop"] = true;
        } else {
            layout["below"] = par.id;
        }
        layout["marginTop"] = source.yCoordinate - par.yCoordinate - par.height;
    } else if (source.yCoordinate > par.yCoordinate) {
        layout["alignTop"] = par.id;
        layout["marginTop"] = source.yCoordinate - par.yCoordinate;
    } else if (par.yCoordinate > source.yCoordinate) {
        layout["alignBottom"] = par.id;
        if (par.yCoordinate + par.height > source.yCoordinate + source.height) {
            layout["marginBottom"] = par.yCoordinate + par.height - source.yCoordinate - source.height;
        } else {
            layout["marginBottom"] = source.yCoordinate + source.height - par.yCoordinate - par.height;
        }

    } else {
        layout["alignTop"] = par.id;
    }

    layout["width"] = source.width;
    layout["height"] = source.height;

    return layout;
}

getStyle(element) {
    return element.zetletStyle;
}

getZetletElement(element, layout, style) {
    return {
        "id": element.id,
        "type": element.type,
        "layout": layout,
        "style": style
    };
}

createZetlet(elements) {
    var zetlet = {};

    var presentation = {};
    var containerObject = {
        "id": "container"
    };
    presentation["container"] = containerObject;
    var zetletElements = [];
    for (var i = 0; i < elements.length - 1; i++) {
        var par = this.parent.get(elements[i]);
        var layout = this.getLayout(elements[i], par);
        var style = this.getStyle(elements[i]);
        var zetletElement = this.getZetletElement(elements[i], layout, style);
        zetletElements.push(zetletElement);
    }
    presentation["elements"] = zetletElements;
    zetlet["presentation"] = presentation;

    var dataTemplate = {};
    for (var i = 0; i < elements.length - 1; i++) {
        var data = elements[i]['data'];
        var ids = elements[i].id;
        dataTemplate[ids] = data;
    }

    zetlet["dataTemplate"] = dataTemplate;
    return zetlet;
}

parseInput(elements) {
    this.createGraph(elements);

    var visited = new Set();
    this.calculateParent(elements[elements.length - 1], visited, null);

    return this.createZetlet(elements);
}
};

/*
function main() {
    var heading1 = new Element(10, 10, 20, 20, "heading1", null, "text", null);
    var heading2 = new Element(30, 5, 50, 50, "heading2", null, "text", null);

    var elements = [];
    elements.push(heading1);
    elements.push(subHeading);
    elements.push(heading2);

    document.write(parseInput(elements));
}

main();

*/

module.exports= ZetletParser


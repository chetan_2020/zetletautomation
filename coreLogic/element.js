
class Element {
    constructor(xCoordinate, yCoordinate, width, height, id, zetletStyle, type, data) {
    	this.xCoordinate = xCoordinate;
    	this.yCoordinate = yCoordinate;
    	this.width = width;
    	this.height = height;
    	this.id = id;
    	this.zetletStyle = zetletStyle;
    	this.type = type;
        this.data = data;
    }
};

module.exports = Element
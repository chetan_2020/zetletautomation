const express = require('express');
const http =require('http');
const hostname='localhost';
const port=3000;
const morgan =require('morgan');
const bodyparser =require('body-parser');
const parserRouter=require('./routes/parserRouter');

const app= express();
app.use(morgan('dev'));
app.use(bodyparser.json());
app.use('/parser',parserRouter);

app.use(express.static(__dirname+ '/public'));

app.use((req,res,next) => 
{
    res.statusCode=200;
    res.setHeader('Content-Type','text/html');
    res.end('./public/index.html');
});

const server= http.createServer(app);

server.listen(port, hostname, () =>
{
    console.log(`Server running at http://${hostname}:${port}`);
});
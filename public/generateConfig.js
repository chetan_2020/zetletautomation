function generateConfig() {
    console.log("Generating configs");
    const API_URL = 'http://localhost:3000/parser';
    parseElementMap();
    const BODY = Object.fromEntries(elementMap);
    axios.post(API_URL, BODY, { headers: {} })
         .then(response => {
              const data = response.data;
              console.log('data', data);
              download("presentation.json",JSON.stringify(data.presentation))
              download("dataTemplate.json",JSON.stringify(data.dataTemplate));
      })
        .catch(error => console.error('On generate config error: ', error));
}

function parseElementMap() {
        var screenRect = document.getElementById('screen').getBoundingClientRect();
        for (const [key, value] of elementMap.entries()) { 
                var node = document.getElementById(key);                
                value['xCoordinate'] = node.getBoundingClientRect().x - screenRect.x;
                value['yCoordinate'] = node.getBoundingClientRect().y - screenRect.y;
                elementMap.set(key,value);
                console.log(value['xCoordinate']);
                console.log(value['yCoordinate']);
         }
}


function download(filename, text) {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
      
        element.style.display = 'none';
        document.body.appendChild(element);
      
        element.click();
      
        document.body.removeChild(element);
      }
      
      
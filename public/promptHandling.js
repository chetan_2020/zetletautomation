var elementMap = new Map();
function showTextPrompt(id) {
    var textPrompt = document.getElementById("textPrompt");
    textPrompt.style.display = "block";
    var element = {};
    if (elementMap.has(id)) {
        element = elementMap.get(id);
    }
    fillDefaultTextPromptValues(element);
}

function showButtonPrompt(id) {
    var buttonPrompt = document.getElementById("buttonPrompt");
    buttonPrompt.style.display = "block";
    var element = {};
    if (elementMap.has(id)) {
        element = elementMap.get(id);
    }
    fillDefaultButtonPromptValues(element);
}

function showImagePrompt(id) {
    var imagePrompt = document.getElementById("imagePrompt");
    imagePrompt.style.display = "block";
    var element = {};
    if (elementMap.has(id)) {
        element = elementMap.get(id);
    }
    fillDefaultImagePromptValues(element);
}

function showLinePrompt(id) {
    var linePrompt = document.getElementById("linePrompt");
    linePrompt.style.display = "block";
    var element = {};
    if (elementMap.has(id)) {
        element = elementMap.get(id);
    }
    fillDefaultLinePromptValues(element);
}

function fillDefaultTextPromptValues(element) {
    if (element['id'] !== undefined) {
        document.getElementById("textPromptId").value = element['id'];
    } else {
        document.getElementById("textPromptId").value = "";
    }
    if (element['width'] !== undefined) {
        document.getElementById("textWidth").value = element['width'];
    } else {
        document.getElementById("textWidth").value = 150;
    }
    if (element['height'] !== undefined) {
        document.getElementById("textHeight").value = element['height'];
    } else {
        document.getElementById("textHeight").value = 30;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingLeft'] !== undefined) {
        document.getElementById("textPromptPaddingLeft").value = element['zetletStyle']['paddingLeft'];
    } else {
        document.getElementById("textPromptPaddingLeft").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingRight'] !== undefined) {
        document.getElementById("textPromptPaddingRight").value = element['zetletStyle']['paddingRight'];
    } else {
        document.getElementById("textPromptPaddingRight").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingTop'] !== undefined) {
        document.getElementById("textPromptPaddingTop").value = element['zetletStyle']['paddingTop'];
    } else {
        document.getElementById("textPromptPaddingTop").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingBottom'] !== undefined) {
        document.getElementById("textPromptPaddingBottom").value = element['zetletStyle']['paddingBottom'];
    } else {
        document.getElementById("textPromptPaddingBottom").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['cornerRadius'] !== undefined) {
        document.getElementById("textPromptCornerRadius").value = element['zetletStyle']['cornerRadius'];
    } else {
        document.getElementById("textPromptCornerRadius").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['borderWidth'] !== undefined) {
        document.getElementById("textPromptBorderWidth").value = element['zetletStyle']['borderWidth'];
    } else {
        document.getElementById("textPromptBorderWidth").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['borderColor'] !== undefined) {
        document.getElementById("textPromptBorderColor").value = element['zetletStyle']['borderColor'];
    } else {
        document.getElementById("textPromptBorderColor").value = "#000000";
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['textColor'] !== undefined) {
        document.getElementById("textPromptTextColor").value = element['zetletStyle']['textColor'];
    } else {
        document.getElementById("textPromptTextColor").value = "#FFFFFF";
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['backgroundColor'] !== undefined) {
        document.getElementById("textPromptBackgroundColor").value = element['zetletStyle']['backgroundColor'];
    } else {
        document.getElementById("textPromptBackgroundColor").value = "#000000";
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['textSize'] !== undefined) {
        document.getElementById("textPromptTextSize").value = element['zetletStyle']['textSize'];
    } else {
        document.getElementById("textPromptTextSize").value = 12;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['font'] !== undefined) {
        document.getElementById("textPromptFont").value = element['zetletStyle']['font'];
    } else {
        document.getElementById("textPromptFont").value = "";
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['lineHeight'] !== undefined) {
        document.getElementById("textPromptLineHeight").value = element['zetletStyle']['lineHeight'];
    } else {
        document.getElementById("textPromptLineHeight").value = "";
    }
    if (element['data'] !== undefined && element['data']['label'] !== undefined) {
        document.getElementById("textPromptLabel").value = element['data']['label'];
    } else {
        document.getElementById("textPromptLabel").value = "";
    }
}

function fillDefaultButtonPromptValues(element) {
    if (element['id'] !== undefined) {
        document.getElementById("buttonPromptId").value = element['id'];
    } else {
        document.getElementById("buttonPromptId").value = "";
    }
    if (element['width'] !== undefined) {
        document.getElementById("buttonWidth").value = element['width'];
    } else {
        document.getElementById("buttonWidth").value = 150;
    }
    if (element['height'] !== undefined) {
        document.getElementById("buttonHeight").value = element['height'];
    } else {
        document.getElementById("buttonHeight").value = 30;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingLeft'] !== undefined) {
        document.getElementById("buttonPromptPaddingLeft").value = element['zetletStyle']['paddingLeft'];
    } else {
        document.getElementById("buttonPromptPaddingLeft").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingRight'] !== undefined) {
        document.getElementById("buttonPromptPaddingRight").value = element['zetletStyle']['paddingRight'];
    } else {
        document.getElementById("buttonPromptPaddingRight").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingTop'] !== undefined) {
        document.getElementById("buttonPromptPaddingTop").value = element['zetletStyle']['paddingTop'];
    } else {
        document.getElementById("buttonPromptPaddingTop").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingBottom'] !== undefined) {
        document.getElementById("buttonPromptPaddingBottom").value = element['zetletStyle']['paddingBottom'];
    } else {
        document.getElementById("buttonPromptPaddingBottom").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['cornerRadius'] !== undefined) {
        document.getElementById("buttonPromptCornerRadius").value = element['zetletStyle']['cornerRadius'];
    } else {
        document.getElementById("buttonPromptCornerRadius").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['borderWidth'] !== undefined) {
        document.getElementById("buttonPromptBorderWidth").value = element['zetletStyle']['borderWidth'];
    } else {
        document.getElementById("buttonPromptBorderWidth").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['borderColor'] !== undefined) {
        document.getElementById("buttonPromptBorderColor").value = element['zetletStyle']['borderColor'];
    } else {
        document.getElementById("buttonPromptBorderColor").value = "#000000";
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['textColor'] !== undefined) {
        document.getElementById("buttonPromptTextColor").value = element['zetletStyle']['textColor'];
    } else {
        document.getElementById("buttonPromptTextColor").value = "#000000";
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['backgroundColor'] !== undefined) {
        document.getElementById("buttonPromptBackgroundColor").value = element['zetletStyle']['backgroundColor'];
    } else {
        document.getElementById("buttonPromptBackgroundColor").value = "#FFFFFF";
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['textSize'] !== undefined) {
        document.getElementById("buttonPromptTextSize").value = element['zetletStyle']['textSize'];
    } else {
        document.getElementById("buttonPromptTextSize").value = 12;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['font'] !== undefined) {
        document.getElementById("buttonPromptFont").value = element['zetletStyle']['font'];
    } else {
        document.getElementById("buttonPromptFont").value = "";
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['lineHeight'] !== undefined) {
        document.getElementById("buttonPromptLineHeight").value = element['zetletStyle']['lineHeight'];
    } else {
        document.getElementById("buttonPromptLineHeight").value = "";
    }
    if (element['data'] !== undefined && element['data']['label'] !== undefined) {
        document.getElementById("buttonPromptLabel").value = element['data']['label'];
    } else {
        document.getElementById("buttonPromptLabel").value = "";
    }
}

function fillDefaultImagePromptValues(element) {
    if (element['id'] !== undefined) {
        document.getElementById("imagePromptId").value = element['id'];
    } else {
        document.getElementById("imagePromptId").value = "";
    }
    if (element['width'] !== undefined) {
        document.getElementById("imageWidth").value = element['width'];
    } else {
        document.getElementById("imageWidth").value = 150;
    }
    if (element['height'] !== undefined) {
        document.getElementById("imageHeight").value = element['height'];
    } else {
        document.getElementById("imageHeight").value = 30;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingLeft'] !== undefined) {
        document.getElementById("imagePromptPaddingLeft").value = element['zetletStyle']['paddingLeft'];
    } else {
        document.getElementById("imagePromptPaddingLeft").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingRight'] !== undefined) {
        document.getElementById("imagePromptPaddingRight").value = element['zetletStyle']['paddingRight'];
    } else {
        document.getElementById("imagePromptPaddingRight").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingTop'] !== undefined) {
        document.getElementById("imagePromptPaddingTop").value = element['zetletStyle']['paddingTop'];
    } else {
        document.getElementById("imagePromptPaddingTop").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['paddingBottom'] !== undefined) {
        document.getElementById("imagePromptPaddingBottom").value = element['zetletStyle']['paddingBottom'];
    } else {
        document.getElementById("imagePromptPaddingBottom").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['cornerRadius'] !== undefined) {
        document.getElementById("imagePromptCornerRadius").value = element['zetletStyle']['cornerRadius'];
    } else {
        document.getElementById("imagePromptCornerRadius").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['borderWidth'] !== undefined) {
        document.getElementById("imagePromptBorderWidth").value = element['zetletStyle']['borderWidth'];
    } else {
        document.getElementById("imagePromptBorderWidth").value = 0;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['borderColor'] !== undefined) {
        document.getElementById("imagePromptBorderColor").value = element['zetletStyle']['borderColor'];
    } else {
        document.getElementById("imagePromptBorderColor").value = "#000000";
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['backgroundColor'] !== undefined) {
        document.getElementById("imagePromptBackgroundColor").value = element['zetletStyle']['backgroundColor'];
    } else {
        document.getElementById("imagePromptBackgroundColor").value = "#FFFFFF";
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['contentType'] !== undefined) {
        document.getElementById("contentType").value = element['zetletStyle']['contentType'];
    } else {
        document.getElementById("contentType").value = "";
    }
    if (element['data'] !== undefined && element['data']['url'] !== undefined) {
        document.getElementById("imageUrl").value = element['data']['url'];
    } else {
        document.getElementById("imageUrl").value = "";
    }
    if (element['data'] !== undefined && element['data']['placeholderUrl'] !== undefined) {
        document.getElementById("placeHolderImageUrl").value = element['data']['placeholderUrl'];
    } else {
        document.getElementById("placeHolderImageUrl").value = "";
    }
}

function fillDefaultLinePromptValues(element) {
    if (element['id'] !== undefined) {
        document.getElementById("linePromptId").value = element['id'];
    } else {
        document.getElementById("linePromptId").value = "";
    }
    if (element['width'] !== undefined) {
        document.getElementById("lineWidth").value = element['width'];
    } else {
        document.getElementById("lineWidth").value = 150;
    }
    if (element['height'] !== undefined) {
        document.getElementById("lineHeight").value = element['height'];
    } else {
        document.getElementById("lineHeight").value = 30;
    }
    if (element['zetletStyle'] !== undefined && element['zetletStyle']['backgroundColor'] !== undefined) {
        document.getElementById("linePromptBackgroundColor").value = element['zetletStyle']['backgroundColor'];
    } else {
        document.getElementById("linePromptBackgroundColor").value = "#FFFFFF";
    }
}

function saveTextDetails(id) {
    console.log(id);
    var textPrompt = document.getElementById("textPrompt");
    var textPromptId = document.getElementById("textPromptId").value;
    var textWidth = document.getElementById("textWidth").value;
    var textHeight = document.getElementById("textHeight").value;
    var textPaddingLeft = document.getElementById("textPromptPaddingLeft").value;
    var textPaddingRight = document.getElementById("textPromptPaddingRight").value;
    var textPaddingTop = document.getElementById("textPromptPaddingTop").value;
    var textPaddingBottom = document.getElementById("textPromptPaddingBottom").value;
    var textPromptCornerRadius = document.getElementById("textPromptCornerRadius").value;
    var textPromptBorderWidth = document.getElementById("textPromptBorderWidth").value;
    var textPromptBorderColor = document.getElementById("textPromptBorderColor").value;
    var textPromptTextColor = document.getElementById("textPromptTextColor").value;
    var textPromptBackgroundColor = document.getElementById("textPromptBackgroundColor").value;
    var textPromptTextSize = document.getElementById("textPromptTextSize").value;
    var textPromptFont = document.getElementById("textPromptFont").value;
    var textPromptLineHeight = document.getElementById("textPromptLineHeight").value;
    var textPromptLabel = document.getElementById("textPromptLabel").value;

    var text = document.getElementById(id);
    if (textPromptId !== undefined && textPromptId.length > 0) {
        var element = {};
        if (elementMap.has(id)) {
            element = elementMap.get(id);
        }
        element["type"]= "text";
        element["id"]= textPromptId;
        var style = {};
        if (textWidth !== undefined && textWidth.length > 0) {
            element["width"]= textWidth;
            $('#' + id).width(textWidth);
        }
        if (textHeight !== undefined && textHeight.length > 0) {
            element["height"]= textHeight;
            $('#' + id).height(textHeight);
        }
        if (textPaddingLeft !== undefined && textPaddingLeft.length > 0) {
            style["paddingLeft"]= textPaddingLeft;
        }
        if (textPaddingRight !== undefined && textPaddingRight.length > 0) {
            style["paddingRight"]= textPaddingRight;
        }
        if (textPaddingTop !== undefined && textPaddingTop.length > 0) {
            style["paddingTop"]= textPaddingTop;
        }
        if (textPaddingBottom !== undefined && textPaddingBottom.length > 0) {
            style["paddingBottom"]= textPaddingBottom;
        }
        if (textPromptCornerRadius !== undefined && textPromptCornerRadius.length > 0) {
            style["cornerRadius"]= textPromptCornerRadius;
            text.style.borderRadius = textPromptCornerRadius;
        }
        if (textPromptBorderWidth !== undefined && textPromptBorderWidth.length > 0) {
            style["borderWidth"]= textPromptBorderWidth;
            text.style.borderWidth = textPromptBorderWidth;
        }
        if (textPromptBorderColor !== undefined && textPromptBorderColor.length > 0) {
            style["borderColor"]= textPromptBorderColor;
            text.style.borderColor = textPromptBorderColor
        }
        if (textPromptTextColor !== undefined && textPromptTextColor.length > 0) {
            style["textColor"]= textPromptTextColor;
            text.style.color = textPromptTextColor;
        }
        if (textPromptBackgroundColor !== undefined && textPromptBackgroundColor.length > 0) {
            style["backgroundColor"]= textPromptBackgroundColor;
            text.style.backgroundColor = textPromptBackgroundColor;
        }
        if (textPromptTextSize !== undefined && textPromptTextSize.length > 0) {
            style["textSize"]= textPromptTextSize;
            text.style.fontSize = textPromptTextSize + 'px';
        }
        if (textPromptFont !== undefined && textPromptFont.length > 0) {
            style["font"]= textPromptFont;
            text.style.fontFamily = textPromptFont;
        }
        if (textPromptLineHeight !== undefined && textPromptLineHeight.length > 0) {
            style["lineHeight"]= textPromptLineHeight;
            text.style.lineHeight = textPromptLineHeight + 'px';
        }
        element["zetletStyle"]= style;
        var data = {};
        if (textPromptLabel !== undefined && textPromptLabel.length > 0) {
            text.innerHTML = textPromptLabel;
            data["label"]= textPromptLabel;
        }
        element["data"]= data;
        elementMap.set(id, element);
        textPrompt.style.display = "none";
        console.log(elementMap);
    } else {
        alert("Invalid Identifier");
    }
}

function saveButtonDetails(id) {
    var buttonPrompt = document.getElementById("buttonPrompt");
    var buttonPromptId = document.getElementById("buttonPromptId").value;
    var buttonWidth = document.getElementById("buttonWidth").value;
    var buttonHeight = document.getElementById("buttonHeight").value;
    var buttonPaddingLeft = document.getElementById("buttonPromptPaddingLeft").value;
    var buttonPaddingRight = document.getElementById("buttonPromptPaddingRight").value;
    var buttonPaddingTop = document.getElementById("buttonPromptPaddingTop").value;
    var buttonPaddingBottom = document.getElementById("buttonPromptPaddingBottom").value;
    var buttonPromptCornerRadius = document.getElementById("buttonPromptCornerRadius").value;
    var buttonPromptBorderWidth = document.getElementById("buttonPromptBorderWidth").value;
    var buttonPromptBorderColor = document.getElementById("buttonPromptBorderColor").value;
    var buttonPromptTextColor = document.getElementById("buttonPromptTextColor").value;
    var buttonPromptBackgroundColor = document.getElementById("buttonPromptBackgroundColor").value;
    var buttonPromptTextSize = document.getElementById("buttonPromptTextSize").value;
    var buttonPromptFont = document.getElementById("buttonPromptFont").value;
    var buttonPromptLineHeight = document.getElementById("buttonPromptLineHeight").value;
    var buttonPromptLabel = document.getElementById("buttonPromptLabel").value;

    var button = document.getElementById(id);

    if (buttonPromptId !== undefined && buttonPromptId.length > 0) {
        var element = {};
        if (elementMap.has(id)) {
            element = elementMap.get(id);
        }
        element["type"]= "button";
        element["id"]= buttonPromptId;
        var style = {};
        if (buttonWidth !== undefined && buttonWidth.length > 0) {
            element["width"]= buttonWidth;
            $('#' + id).width(buttonWidth);
        }
        if (buttonHeight !== undefined && buttonHeight.length > 0) {
            element["height"]= buttonHeight;
            $('#' + id).height(buttonHeight);
        }
        if (buttonPaddingLeft !== undefined && buttonPaddingLeft.length > 0) {
            style["paddingLeft"]= buttonPaddingLeft;
        }
        if (buttonPaddingRight !== undefined && buttonPaddingRight.length > 0) {
            style["paddingRight"]= buttonPaddingRight;
        }
        if (buttonPaddingTop !== undefined && buttonPaddingTop.length > 0) {
            style["paddingTop"]= buttonPaddingTop;
        }
        if (buttonPaddingBottom !== undefined && buttonPaddingBottom.length > 0) {
            style["paddingBottom"]= buttonPaddingBottom;
        }
        if (buttonPromptCornerRadius !== undefined && buttonPromptCornerRadius.length > 0) {
            style["cornerRadius"]= buttonPromptCornerRadius;
        }
        if (buttonPromptBorderWidth !== undefined && buttonPromptBorderWidth.length > 0) {
            style["borderWidth"]= buttonPromptBorderWidth;
            button.style.borderWidth = buttonPromptBorderWidth;
        }
        if (buttonPromptBorderColor !== undefined && buttonPromptBorderColor.length > 0) {
            style["borderColor"]= buttonPromptBorderColor;
            button.style.borderColor = buttonPromptBorderColor;
        }
        if (buttonPromptTextColor !== undefined && buttonPromptTextColor.length > 0) {
            style["textColor"]= buttonPromptTextColor;
            button.style.color = buttonPromptTextColor;
        }
        if (buttonPromptBackgroundColor !== undefined && buttonPromptBackgroundColor.length > 0) {
            style["backgroundColor"]= buttonPromptBackgroundColor;
            button.style.backgroundColor = buttonPromptBackgroundColor;
        }
        if (buttonPromptTextSize !== undefined && buttonPromptTextSize.length > 0) {
            style["textSize"]= buttonPromptTextSize;
            button.style.fontSize = buttonPromptTextSize + 'px';
        }
        if (buttonPromptFont !== undefined && buttonPromptFont.length > 0) {
            style["font"]= buttonPromptFont;
            button.style.fontFamily = buttonPromptFont;
        }
        if (buttonPromptLineHeight !== undefined && buttonPromptLineHeight.length > 0) {
            style["lineHeight"]= buttonPromptLineHeight;
            button.style.lineHeight = buttonPromptLineHeight + 'px';
        }
        element["zetletStyle"]= style;
        var data = {};
        if (buttonPromptLabel !== undefined && buttonPromptLabel.length > 0) {
            data["label"]= buttonPromptLabel;
            button.innerHTML = buttonPromptLabel;
        }
        element["data"]= data;
        elementMap.set(id, element);
        buttonPrompt.style.display = "none";
        console.log(elementMap);
    } else {
        alert("Invalid Identifier");
    }
}

function saveImageDetails(id) {
    var imagePrompt = document.getElementById("imagePrompt");
    var imagePromptId = document.getElementById("imagePromptId").value;
    var imageWidth = document.getElementById("imageWidth").value;
    var imageHeight = document.getElementById("imageHeight").value;
    var imagePaddingLeft = document.getElementById("imagePromptPaddingLeft").value;
    var imagePaddingRight = document.getElementById("imagePromptPaddingRight").value;
    var imagePaddingTop = document.getElementById("imagePromptPaddingTop").value;
    var imagePaddingBottom = document.getElementById("imagePromptPaddingBottom").value;
    var imagePromptCornerRadius = document.getElementById("imagePromptCornerRadius").value;
    var imagePromptBorderWidth = document.getElementById("imagePromptBorderWidth").value;
    var imagePromptBorderColor = document.getElementById("imagePromptBorderColor").value;
    var contentType = document.getElementById("contentType").value;
    var imageUrl = document.getElementById("imageUrl").value;
    var placeHolderImageUrl = document.getElementById("placeHolderImageUrl").value;

    var image = document.getElementById(id);

    if (imagePromptId !== undefined && imagePromptId.length > 0) {
        var element = {};
        if (elementMap.has(id)) {
            element = elementMap.get(id);
        }
        element["type"]= "image";
        element["id"]= imagePromptId;
        var style = {};
        if (imageWidth !== undefined && imageWidth.length > 0) {
            element["width"]= imageWidth;
            $('#' + id).width(imageWidth);
        }
        if (imageHeight !== undefined && imageHeight.length > 0) {
            element["height"]= imageHeight;
            $('#' + id).height(imageHeight);
        }
        if (imagePaddingLeft !== undefined && imagePaddingLeft.length > 0) {
            style["paddingLeft"]= imagePaddingLeft;
        }
        if (imagePaddingRight !== undefined && imagePaddingRight.length > 0) {
            style["paddingRight"]= imagePaddingRight;
        }
        if (imagePaddingTop !== undefined && imagePaddingTop.length > 0) {
            style["paddingTop"]= imagePaddingTop;
        }
        if (imagePaddingBottom !== undefined && imagePaddingBottom.length > 0) {
            style["paddingBottom"]= imagePaddingBottom;
        }
        if (imagePromptCornerRadius !== undefined && imagePromptCornerRadius.length > 0) {
            style["cornerRadius"]= imagePromptCornerRadius;
            image.style.borderRadius = imagePromptCornerRadius;
        }
        if (imagePromptBorderWidth !== undefined && imagePromptBorderWidth.length > 0) {
            style["borderWidth"]= imagePromptBorderWidth;
            image.style.borderWidth = imagePromptBorderWidth;
        }
        if (imagePromptBorderColor !== undefined && imagePromptBorderColor.length > 0) {
            style["borderColor"]= imagePromptBorderColor;
            image.style.borderColor = imagePromptBorderColor;
        }
        if (contentType !== undefined && contentType.length > 0) {
            style["contentType"]= contentType;
        }
        if (imagePromptBackgroundColor !== undefined && imagePromptBackgroundColor.length > 0) {
            style["backgroundColor"]= imagePromptBackgroundColor;
            image.style.backgroundColor = imagePromptBackgroundColor;
        }
        element["zetletStyle"]= style;
        var data = {};
        if (imageUrl !== undefined && imageUrl.length > 0) {
            data["url"]= imageUrl;
        }
        if (placeHolderImageUrl !== undefined && placeHolderImageUrl.length > 0) {
            data["placeholderUrl"]= placeHolderImageUrl;
        }
        element["data"]= data;
        elementMap.set(id, element);
        imagePrompt.style.display = "none";
        console.log(elementMap);
    } else {
        alert("Invalid Identifier");
    }
}

function saveLineDetails(id) {
    var linePrompt = document.getElementById("linePrompt");
    var linePromptId = document.getElementById("linePromptId").value;
    var lineWidth = document.getElementById("lineWidth").value;
    var lineHeight = document.getElementById("lineHeight").value;
    var linePromptBackgroundColor = document.getElementById("linePromptBackgroundColor").value;

    var line = document.getElementById(id);
    line.innerHTML = "";

    if (linePromptId !== undefined && linePromptId.length > 0) {
        var element ={};
        if (elementMap.has(id)) {
            element = elementMap.get(id);
        }
        element["type"]= "line";
        element["id"]= linePromptId;
        var style = {};
        if (lineWidth !== undefined && lineWidth.length > 0) {
            element["width"]= lineWidth;
            $('#' + id).width(lineWidth);
        }
        if (lineHeight !== undefined && lineHeight.length > 0) {
            element["height"]= lineHeight;
            $('#' + id).height(lineHeight);
        }
        if (linePromptBackgroundColor !== undefined && linePromptBackgroundColor.length > 0) {
            style["backgroundColor"]= linePromptBackgroundColor;
            line.style.backgroundColor = linePromptBackgroundColor;
        }
        element["zetletStyle"]= style;
        elementMap.set(id, element);
        linePrompt.style.display = "none";
        console.log(elementMap);
    } else {
        alert("Invalid Identifier");
    }
}

window.onclick = function(event) {
  var textPrompt = document.getElementById("textPrompt");
  var buttonPrompt = document.getElementById("buttonPrompt");
  var imagePrompt = document.getElementById("imagePrompt");
  var linePrompt = document.getElementById("linePrompt");
  if (event.target == textPrompt) {
    textPrompt.style.display = "none";
  }
  if (event.target == buttonPrompt) {
    buttonPrompt.style.display = "none";
  }
  if (event.target == imagePrompt) {
    imagePrompt.style.display = "none";
  }
  if (event.target == linePrompt) {
    linePrompt.style.display = "none";
  }
}

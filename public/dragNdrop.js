
/* events fired on the draggable target */
document.addEventListener("drag", function(event) {

}, false);

document.addEventListener("dragstart", function(event) {
  // store a ref. on the dragged elem
  event.dataTransfer.setData("text", event.target.id);
  // make it half transparent
}, false);

document.addEventListener("dragend", function(event) {
  // reset the transparency
}, false);

/* events fired on the drop targets */
document.addEventListener("dragover", function(event) {
  // prevent default to allow drop
  event.preventDefault();
}, false);

document.addEventListener("dragenter", function(event) {
  // highlight potential drop target when the draggable element enters it
  if (event.target.className == "screens") {
    event.target.style.opacity = 0.5;
  }

}, false);

document.addEventListener("dragleave", function(event) {
  // reset background of potential drop target when the draggable element leaves it
  if (event.target.className == "screens") {
    event.target.style.opacity = 1;
  }

}, false);

document.addEventListener("drop", function(event) {
  // prevent default action (open as link for some elements)
  event.preventDefault();
  // move dragged elem to the selected drop target
  if (event.target.className == "screens") {
    event.target.style.opacity = 1;
    var data = event.dataTransfer.getData("text");
    console.log(data);
    //event.target.style.background = "";
    //dragged.parentNode.removeChild( dragged );
   Drop(event,data);
  }
}, false);


function Drop(event,draggableId) {   
  if(draggableId !== 'text' && draggableId !== 'button' && draggableId !== 'image' && draggableId !== 'line') {
    var node = document.getElementById(draggableId);
    node.parentElement.removeChild(node);
    event.target.appendChild(node);
    placeDiv(event.clientX,event.clientY,draggableId);
    return;
  }
  var nodeCopy = document.getElementById(draggableId).cloneNode(true);
   nodeCopy.id = getId(draggableId);
   nodeCopy.classList.remove("items");
   nodeCopy.classList.add("newItems");
   event.target.appendChild(nodeCopy);
   placeDiv(event.clientX,event.clientY,nodeCopy.id);
   //nodeCopy.ondragstart = dropNewItems();
  if(draggableId === 'text') {
     nodeCopy.onclick = function() {
       showTextPrompt(nodeCopy.id);
     }
     var button = document.getElementById("textSubmitButton");
     button.onclick = function() {
     saveTextDetails(nodeCopy.id);
   }
  } else if(draggableId === 'button') {
     nodeCopy.onclick = function() {
       showButtonPrompt(nodeCopy.id);
     }
     var button = document.getElementById("buttonSubmitButton");
     button.onclick = function() {
     saveButtonDetails(nodeCopy.id);
 }
  } else if(draggableId === 'image') {
     nodeCopy.onclick = function() {
       showImagePrompt(nodeCopy.id);
     }  
     var button = document.getElementById("imageSubmitButton");
     button.onclick = function() {
     saveImageDetails(nodeCopy.id);
   }
  } else {
     nodeCopy.onclick = function() {
       showLinePrompt(nodeCopy.id);
     }
     var button = document.getElementById("lineSubmitButton");
     button.onclick = function() {
     saveLineDetails(nodeCopy.id);
   }
  }
}

function getId(id) {
  return id + Math.floor(Math.random() * 100000);
}

function placeDiv(x_pos, y_pos, id) {
  var d = document.getElementById(id);
  d.style.position = "absolute";
  d.style.left = x_pos+'px';
  d.style.top = y_pos+'px';
}
